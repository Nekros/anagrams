from collections import defaultdict
import hashlib


MIN_WORLD_LENGTH = 4


def calc_hash(words_list, dictionary, ind=0, words=None):

    words = [''] * len(words_list) if words is None else words

    if ind == len(words_list):
        sentence = ' '.join(words)
        mh = hashlib.md5(sentence.encode('utf-8')).hexdigest()
        if mh in ['e4820b45d2277f3844eac66c903e84be', '23170acc097c24edb98fc5488ab033fe', '665e5bcb0c20062fe8abaaf4628bb154']:
            print(mh + '. words: ' + sentence)
        return

    for chance in dictionary[words_list[ind]][1]:
        words[ind] = chance
        calc_hash(words_list, dictionary, ind+1, words)



def ruc(x):
    return x.replace('\'', '')


def dict_freq(chars):
    d = defaultdict(lambda: 0)
    for i in chars:
        d[i] += 1
    return d


def word_writable(what, where):

    for v, k in what.items():
        if v not in where or what[v] > where[v]:
            return False
    return True


def build_dictionary(file_wordlist, freq_cs, mwl):
    words = defaultdict(lambda: (None, set()))
    with open(file_wordlist) as f:
        for x in f:
            mw = x.strip()
            # ruc is just a function that removes apostrophes from words
            p = ruc(mw).lower()
            if len(p) >= mwl:
                if p not in words:
                    # dict_freq returns the frequencies dictionary
                    freq_p = dict_freq(p)
                    # word_writable, given frequencies of words a and b, gives true if a can be written with b chars
                    if word_writable(freq_p, freq_cs):
                        words[p] = (freq_p, set())
                        words[p][1].add(mw)
                else:
                    words[p][1].add(mw)

    return words


class Node(object):
    def __init__(self, letter='', path='', final=False, depth=0):
        self.letter = letter
        self.final = final
        self.depth = depth
        self.children = {}
        self.path = path

    def add(self, letters):
        node = self
        for index, letter in enumerate(letters):
            if letter not in node.children:
                node.children[letter] = Node(letter, letters[:index+1], index==len(letters)-1, index+1)
            node = node.children[letter]


def create_trie(wordlist):
    trie = Node()
    for x in wordlist:
        trie.add(x)
    return trie


def last(freq):
    for k, v in freq.items():
        if v != 0:
            return False
    return True


def count_freq(freq):
    return sum(v for k, v in freq.items())



def calc(current_node, fcs, ind = 0):

    if current_node.final:
        global counter
        global chars_used

        words[ind] = current_node.path
        chars_used += len(current_node.path)
        # chars_used = sum(len(z) for z in words[:ind+1])
        if lcs == chars_used:  # sentence complete

            counter += 1
            if not counter % 10000:
                print(counter)
                print(words[:ind+1])
            calc_hash(words[:ind+1], my_dict)
            chars_used -= len(current_node.path)
            return

        elif lcs - chars_used >= MIN_WORLD_LENGTH:
            calc(trie, fcs, ind + 1)
            chars_used -= len(current_node.path)
        else:
            chars_used -= len(current_node.path)
            return

    for x in fcs:
        if fcs[x] > 0 and x in current_node.children:
            fcs[x] -= 1
            calc(current_node.children[x], fcs, ind)
            fcs[x] += 1

counter = 0
chars_used = 0

if __name__ == '__main__':
    cs = ''.join([x for x in 'poultry outwits ants' if x != ' '])
    lcs = len(cs)
    fcs = dict_freq(cs)
    my_dict = build_dictionary('wordlist', fcs, MIN_WORLD_LENGTH)

    words = [''] * (lcs // MIN_WORLD_LENGTH)

    trie = create_trie(my_dict)
    calc(trie, fcs)

