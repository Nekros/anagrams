from collections import defaultdict
import hashlib


def convert_wl(file_wordlist):
    with open(file_wordlist) as f:
        for x in f:
            yield x.strip()


def calc_hash(words_list, dictionary, ind=0, words=None):

    words = [''] * len(words_list) if words is None else words

    if ind == len(words_list):
        sentence = ' '.join(words)
        mh = hashlib.md5(sentence.encode('utf-8')).hexdigest()
        if mh in get_hashes():
            print(mh + '. words: ' + sentence)
        return

    for chance in dictionary[words_list[ind]][1]:
        words[ind] = chance
        calc_hash(words_list, dictionary, ind+1, words)


def ruc(x):
    return x.replace('\'', '')


def get_hashes():
    return ['e4820b45d2277f3844eac66c903e84be', '23170acc097c24edb98fc5488ab033fe', '665e5bcb0c20062fe8abaaf4628bb154']


def get_characters(sentence):
    return ''.join([x for x in sentence if x != ' '])


def dict_freq(chars):
    d = defaultdict(lambda: 0)
    for i in chars:
        d[i] += 1
    return d


def sub_dict_freq(what, where):
    for k, v in what.items():
        where[k] -= what[k]
    return where


def add_dict_freq(what, where):
    for k, v in what.items():
        where[k] += what[k]
    return where


def word_writable(what, where):

    for v, k in what.items():
        if v not in where or what[v] > where[v]:
            return False
    return True

def build_dictionary(wordlist, freq_cs, mwl):
    words = defaultdict(lambda: (None, set()))

    for x in wordlist:
        mw = x.strip()
        # ruc is just a function that removes apostrophes from words
        p = ''.join(sorted(ruc(mw).lower()))
        if len(p) >= mwl:
            if p not in words:
                # dict_freq returns the frequencies dictionary
                freq_p = dict_freq(p)
                # word_writable, given frequencies of words a and b, gives true if a can be written with b chars
                if word_writable(freq_p, freq_cs):
                    words[p] = (freq_p, set())
                    words[p][1].add(mw)
            else:
                words[p][1].add(mw)

    return words


def get_wordlist(wordlist, freq_cs):

    removed = defaultdict(lambda: (None, set()))

    for x in list(wordlist.keys()):
        if not word_writable(wordlist[x][0], freq_cs):
            removed[x] = wordlist.pop(x)

    return removed
