from utility import get_characters, get_wordlist, calc_hash, convert_wl, dict_freq, sub_dict_freq, add_dict_freq, build_dictionary
import copy


MIN_WORLD_LENGTH = 3
counter = 0
cs = get_characters('poultry outwits ants')
freq_cs = dict_freq(cs)
wordlist = build_dictionary(convert_wl('wordlist'), freq_cs, MIN_WORLD_LENGTH)

words = [''] * (len(cs)//MIN_WORLD_LENGTH)


def last(freq):
    for k, v in freq.items():
        if v != 0:
            return False
    return True


def calc_b(dc, wl, ind=0):
    # last calculates if frequencies dictionary has all values to 0 => sentence is complete
    if last(dc):
        global counter
        counter += 1
        if not counter % 10000:
            print(counter)
            print(words[:ind])
        calc_hash(words[:ind], wordlist)
        return

    # get all possible word that can be used given current frequencies
    missing = get_wordlist(wl, dc)
    cc = list(wl.keys())
    for word in cc:
        words[ind] = word
        # For every word, subtract frequencies and go with recursive approach
        calc_b(sub_dict_freq(wordlist[word][0], dc), wl, ind+1)
        # word used, add again frequencies and try a new word
        add_dict_freq(wordlist[word][0], dc)
    wl.update(missing)


def main():
    calc_b(freq_cs, copy.copy(wordlist))


if __name__ == '__main__':
    main()
